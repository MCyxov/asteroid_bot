from PyQt5.QtGui import QColor, QImage


class AddHealth1:
    def __init__(self, x, y, r):
        self.x = x
        self.r = r
        self.y = y
        self.speed_x = 0
        self.speed_y = 0

    def draw(self, painter):
        xd = self.x - 15
        yd = self.y - 15
        painter.drawImage(xd, yd, QImage('texture/health.png'))

    def on_collision(self, robot):
        robot.health += 1


class AddAmmo1:
    def __init__(self, x, y, r):
        self.x = x
        self.r = r
        self.y = y
        self.speed_x = 0
        self.speed_y = 0

    def draw(self, painter):
        xd = self.x - 15
        yd = self.y - 15
        painter.drawImage(xd, yd, QImage('texture/ammo.png'))

    def on_collision(self, robot):
        robot.all_ammunition += 30


class AddMany:
    def __init__(self, x, y, r):
        self.x = x
        self.r = r
        self.y = y
        self.speed_x = 0
        self.speed_y = 0

    def draw(self, painter):
        xd = self.x - 15
        yd = self.y - 15
        painter.drawImage(xd, yd, QImage('texture/coin.png'))

    def on_collision(self, robot):
        robot.many += 1
