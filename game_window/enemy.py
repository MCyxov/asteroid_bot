import math

from PyQt5.QtGui import QImage

from game_window.object import SimpleBullets, EnemyBullets, ParticleOfBlood


class Stranger:
    def __init__(self, x, y, r, health):
        self.x = y
        self.y = x
        self.r = r
        self.speed_x = 0
        self.speed_y = 0
        self.reloading = 0
        self.heals = health
        self.img1_r = QImage('texture/alien_right.png')

    def draw(self, painter):
        xd = self.x - 15
        yd = self.y - 15
        painter.drawImage(xd, yd, self.img1_r)

    def update(self):
        pass

    def destroy(self, game):
        return None

    def shot(self, game):
        self.reloading += 1
        if self.reloading < 15:
            return
        self.reloading = 0
        x1 = self.x
        y1 = self.y
        x2 = game.dynamic_field[0].x
        y2 = game.dynamic_field[0].y
        vx = (x2 - x1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        vy = (y2 - y1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        self.speed_x -= vx
        self.speed_y -= vy
        game.bullets.append(EnemyBullets(x1, y1, vx, vy))

    def hit(self, game):
        if self.heals < 0:
            for i in range(10):
                game.particle.append(ParticleOfBlood(self.x + i - 5, self.y + i - 5))
            return True
        else:
            self.heals -= game.dynamic_field[0].gun.type_bullets.damage
        return False
