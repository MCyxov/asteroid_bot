from game_window.object import Empty


class Field:
    def __init__(self, game, width=20, height=20):
        self.width = width
        self.height = height
        self.game = game

        self.cells = [[Empty(j, i) for j in range(width)] for i in range(height)]
        self.hard_cells = []

    def contains_cell(self, y, x):
        return (0 <= y < self.height) and (0 <= x < self.width)

    def is_empty(self, y, x):
        if not self.contains_cell(y, x):
            raise ValueError('Cell ({0}, {1}) is outside of field'.format(y, x))
        return self.cells[y][x] is None

    def set_cell(self, y, x, cell):
        if not self.contains_cell(y, x):
            raise ValueError('Cell ({0}, {1}) is outside of field'.format(y, x))
        self.cells[y][x] = cell
        if cell.id != 1:
            self.hard_cells.append(cell)

    def get_cell(self, y, x):
        if not self.contains_cell(y, x):
            raise ValueError('Cell ({0}, {1}) is outside of field'.format(y, x))
        return self.cells[y][x]

    def update(self, game):
        self.game = game
        for y in range(self.height - 1, -1, -1):
            for x in range(self.width):
                cell = self.cells[y][x]
                if cell is not None:
                    self.cells[y][x] = cell.update()

    def possible_to_go_for_point(self, x, y):
        for i in range(1, len(self.game.dynamic_field)):
            if self.game.is_enemy != i:
                if int(self.game.dynamic_field[i].x / 30) == int(x / 30) and int(self.game.dynamic_field[i].y / 30) == int(y / 30):
                    if self.game.id_bullets[0] != 51:
                        self.game.bullets_delete = True
                        if self.game.dynamic_field[i].hit(self.game):
                            print(self.game.dynamic_field[i].heals)
                            self.game.dynamic_field.pop(i)
                    elif self.game.id_bullets[0] == 51:
                        return True
                    return False
        if x >= (self.width - 1) * 30 or y >= self.height * 30 or x <= 0 or y <= 0:
            return True
        if self.get_cell(int(y / 30), int(x / 30)).id == 1:
            return True
        return False

    def possible_to_go(self, x, y, r):
        # проверка кратных 90
        if (self.possible_to_go_for_point(x, y + r) and self.possible_to_go_for_point(x + r, y)
                and self.possible_to_go_for_point(x - r, y) and self.possible_to_go_for_point(x, y - r)):
            # проверка кратных 45
            if (self.possible_to_go_for_point(x + r, y + r) and self.possible_to_go_for_point(x + r, y - r)
                    and self.possible_to_go_for_point(x - r, y + r) and self.possible_to_go_for_point(x - r, y - r)):
                return True
        return False

    def destroy(self, x, y, game):
        self.cells[int(y / 30)][int(x / 30)] = self.get_cell(int(y / 30), int(x / 30)).destroy(game)
