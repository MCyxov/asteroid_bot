import copy
import math

from game_window.all_bonus import *
from game_window.enemy import Stranger
from game_window.field import Field
from game_window.gun import Pistol, Firearm, MachineGun
from game_window.init_level import init_level
from game_window.object import ParticleOfDirt
from game_window.robot import Robot
from inventory_window.inventory_window import InventoryWindow
from shop.gui import Board


class Game:

    friction_koef = 0.8
    pump_koef = 0.7
    g = 0.008
    center = (300, 300)

    def __init__(self, height, wight):
        self.game_status = 1
        self.inventory = InventoryWindow()
        self.window = None
        self.height = height
        self.wight = wight
        self.count_y = 30
        self.bullets_delete = False
        self.is_paused = False
        self.field = Field(width=self.wight, height=self.height, game=self)
        self.id_bullets = [51, 0]
        self.is_enemy = 0
        self.dynamic_field = []
        self.particle = []
        self.bullets = []
        self.bonus = []

    def start(self, window, n, gui, robot=None):
        self.gui = gui
        self.window = window
        self.game_status = n
        if robot is None:
            init_level(self, n, Robot(300, 40, 13, self.window))
        else:
            robot_n = copy.copy(robot)
            robot_n.x = 300
            robot_n.y = 40
            init_level(self, n, robot_n)

    def break_of_robot(self):
        self.dynamic_field[0].speed_of_x = 0
        self.dynamic_field[0].speed_of_y = 0

    def update(self):
        if self.is_paused:
            self.break_of_robot()
            return
        self.real_go()
        x, y = self.gravity(self.dynamic_field[0].x, self.dynamic_field[0].y)
        self.dynamic_field[0].speed_of_x += x
        self.dynamic_field[0].speed_of_y += y
        self.physics_of_particle()
        self.physics_of_bullets()
        self.physics_of_enemy()
        self.physics_of_bonus()

    def pause(self):
        self.is_paused = not self.is_paused

    def go(self, how):
        PATTERNS = {
            'u': (0, -1),
            'd': (0, 1),
            'l': (-1, 0),
            'r': (1, 0),
        }

        SPEED_X = PATTERNS[how][0]
        SPEED_Y = PATTERNS[how][1]

        if self.dynamic_field[0].speed_of_x < 30:
            self.dynamic_field[0].speed_of_x += SPEED_X
        if self.dynamic_field[0].speed_of_y < 30:
            self.dynamic_field[0].speed_of_y += SPEED_Y

    def real_go(self):
        if 0.05 < self.dynamic_field[0].speed_of_x < 0.05:
            self.dynamic_field[0].speed_of_x = 0
        if 0.05 < self.dynamic_field[0].speed_of_y < 0.05:
            self.dynamic_field[0].speed_of_y = 0
        if self.field.possible_to_go(self.dynamic_field[0].x + self.dynamic_field[0].speed_of_x,
                                     self.dynamic_field[0].y + self.dynamic_field[0].speed_of_y, self.dynamic_field[0].r):
            self.dynamic_field[0].x += self.dynamic_field[0].speed_of_x
            self.dynamic_field[0].y += self.dynamic_field[0].speed_of_y
        else:
            if self.field.possible_to_go(self.dynamic_field[0].x + self.dynamic_field[0].speed_of_x,
                                         self.dynamic_field[0].y, self.dynamic_field[0].r):
                self.dynamic_field[0].x += self.dynamic_field[0].speed_of_x
                self.dynamic_field[0].speed_of_x *= self.friction_koef
                self.dynamic_field[0].speed_of_y = 0
            elif self.field.possible_to_go(self.dynamic_field[0].x,
                                           self.dynamic_field[0].y + self.dynamic_field[0].speed_of_y, self.dynamic_field[0].r):
                self.dynamic_field[0].y += self.dynamic_field[0].speed_of_y
                self.dynamic_field[0].speed_of_y *= self.friction_koef
                self.dynamic_field[0].speed_of_x = 0
            else:
                self.dynamic_field[0].speed_of_x = 0
                self.dynamic_field[0].speed_of_y = 0

    def gravity(self, x, y):
        g_x = 0
        g_y = 0
        for i in self.field.hard_cells:
            c_x = i.x * 30 + 15
            c_y = i.y * 30 + 15
            g_x0 = -x + c_x
            g_y0 = -y + c_y
            if math.sqrt(g_x0 * g_x0 + g_y0 * g_y0) * self.g * i.m != 0:
                g_x += g_x0 / (math.sqrt(g_x0 * g_x0 + g_y0 * g_y0)) * self.g * i.m
                g_y += g_y0 / (math.sqrt(g_x0 * g_x0 + g_y0 * g_y0)) * self.g * i.m
        return g_x, g_y

    def destroy(self):
        x = self.dynamic_field[0].x
        y = self.dynamic_field[0].y
        if not self.field.possible_to_go_for_point(x + 16, y):
            self.field.destroy(x + 16, y, self)
            self.add_particle(x + 16, y)
        if not self.field.possible_to_go_for_point(x - 16, y):
            self.field.destroy(x - 16, y, self)
            self.add_particle(x - 16, y)
        if not self.field.possible_to_go_for_point(x, y + 16):
            self.field.destroy(x, y + 16, self)
            self.add_particle(x, y + 16)
        if not self.field.possible_to_go_for_point(x, y - 16):
            self.field.destroy(x, y - 16, self)
            self.add_particle(x, y - 16)

    def physics_of_particle(self):
        for i in self.particle:
            x, y = self.gravity(i.x, i.y)
            i.speed_x += x
            i.speed_y += y
            if self.field.possible_to_go_for_point(i.x + i.speed_x, i.y + i.speed_y):
                i.x += i.speed_x
                i.y += i.speed_y
            elif self.field.possible_to_go_for_point(i.x + i.speed_x, i.y):
                i.x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go_for_point(i.x, i.y + i.speed_y):
                i.speed_x = -i.speed_x * self.pump_koef
                i.y += i.speed_y
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if i.update() is None:
                self.particle.pop(self.particle.index(i))

    def add_particle(self, x, y):
        self.particle.append(ParticleOfDirt(x, y))

    def shot(self, x2, y2):
        k = self.dynamic_field[0].shot()
        for i in range(k):
            self.dynamic_field[0].all_ammunition -= 1
            x1 = self.dynamic_field[0].x
            y1 = self.dynamic_field[0].y
            vx = (x2 - x1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            vy = (y2 - y1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            self.dynamic_field[0].speed_of_x -= vx
            self.dynamic_field[0].speed_of_y -= vy
            self.bullets.append(self.dynamic_field[0].gun.type_bullets(x1 - vx * i * 10, y1 - vy * i * 10, vx, vy))

    def physics_of_bullets(self):
        for i in self.bullets:
            self.bullets_delete = False
            self.id_bullets = [i.id, self.bullets.index(i)]
            x, y = self.gravity(i.x, i.y)
            i.speed_x += x
            i.speed_y += y
            if i.update() is None:
                self.bullets.pop(self.bullets.index(i))
                continue
            if self.field.possible_to_go_for_point(i.x + i.speed_x, i.y + i.speed_y):
                i.x += i.speed_x
                i.y += i.speed_y
            elif self.field.possible_to_go_for_point(i.x + i.speed_x, i.y):
                i.x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go_for_point(i.x, i.y + i.speed_y):
                i.speed_x = -i.speed_x * self.pump_koef
                i.y += i.speed_y
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if self.collision_with_robot(i.x, i.y) and self.id_bullets[0] == 51:
                self.bullets_delete = True
                self.dynamic_field[0].hit(self)
            if self.bullets_delete:
                self.bullets.pop(self.id_bullets[1])
        self.id_bullets = [51, 0]

    def physics_of_enemy(self):
        for j in range(1, len(self.dynamic_field)):
            self.is_enemy = j
            i = self.dynamic_field[j]
            x, y = self.gravity(i.x, i.y)
            i.speed_x += x
            i.speed_y += y
            if self.field.possible_to_go(i.x + i.speed_x, i.y + i.speed_y, i.r):
                self.dynamic_field[j].x += i.speed_x
                self.dynamic_field[j].y += i.speed_y
            elif self.field.possible_to_go(i.x + i.speed_x, i.y, i.r):
                self.dynamic_field[j].x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go(i.x, i.y + i.speed_y, i.r):
                i.speed_x = -i.speed_x * self.pump_koef
                self.dynamic_field[j].y += i.speed_y
            else:
                i.speed_y = 0
                i.speed_x = 0
            i.shot(self)
        self.is_enemy = -1

    def physics_of_bonus(self):
        for j in range(len(self.bonus)):
            try:
                i = self.bonus[j]
            except IndexError:
                continue
            x, y = self.gravity(i.x, i.y)
            i.speed_x += x
            i.speed_y += y
            if self.field.possible_to_go(i.x + i.speed_x, i.y + i.speed_y, i.r):
                self.bonus[j].x += i.speed_x
                self.bonus[j].y += i.speed_y
            elif self.field.possible_to_go(i.x + i.speed_x, i.y, i.r):
                self.bonus[j].x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go(i.x, i.y + i.speed_y, i.r):
                i.speed_x = -i.speed_x
                self.bonus[j].y += i.speed_y * self.pump_koef
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if self.collision_with_robot(self.bonus[j].x, self.bonus[j].y, i.r):
                self.bonus[j].on_collision(self.dynamic_field[0])
                self.bonus.pop(j)

    def spawn(self, x, y, spawn_type):
        if spawn_type == 'health':
            self.bonus.append(AddHealth1(y + 15, x + 15, 5))
        elif spawn_type == 'bullets':
            self.bonus.append(AddAmmo1(y + 15, x + 15, 5))
        elif spawn_type == 'Simple enemy':
            self.dynamic_field.append(Stranger(x + 15, y + 15, 10, 20))
        elif spawn_type == 'Firearm':
            self.bonus.append(Firearm(y + 15, x + 15, 5))
        elif spawn_type == 'Pistol':
            self.bonus.append(Pistol(y + 15, x + 15, 5))
        elif spawn_type == 'machinegun':
            self.bonus.append(MachineGun(y + 15, x + 15, 5))
        elif spawn_type == 'many':
            self.bonus.append(AddMany(y + 15, x + 15, 5))

    def game_over(self):
        self.is_paused = True
        self.game_status = -1

    def collision_with_robot(self, xp, yp, r=0):
        xr = self.dynamic_field[0].x
        yr = self.dynamic_field[0].y
        if (xr - xp) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp + r) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp) ** 2 + (yr - yp + r) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp - r) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp) ** 2 + (yr - yp - r) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        return False

    def move_to(self, x2, y2):
        x1 = self.dynamic_field[0].x
        y1 = self.dynamic_field[0].y
        vx = (x2 - x1)
        vy = (y2 - y1)
        self.dynamic_field[0].speed_of_x += vx // 35
        self.dynamic_field[0].speed_of_y += vy // 35

    def next_level(self):
        self.win = Board(self, self.dynamic_field[0], self.game_status)
        self.window.close()
        self.gui.timer.stop()
        self.gui.close()
