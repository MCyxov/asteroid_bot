from PyQt5.QtGui import QImage

from game_window.object import *


class Pistol:
    id = 60
    img = QImage('texture/pistol.png')

    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r
        self.speed_x = 0
        self.speed_y = 0
        self.type_bullets = MiniBullets
        self.reloading = 50
        self.ammunition = 6

    def possible_to_shot(self):
        if self.ammunition < 1:
            if self.reloading < 1:
                self.reloading = 50
                self.ammunition = 6
                return 1
            return 0
        self.ammunition -= 1
        self.reloading = 50
        return 1

    def draw(self, painter):
        dx = self.x - 15
        dy = self.y - 15
        painter.drawImage(dx, dy, self.img)

    def update(self):
        if self.ammunition < 1:
            if self.reloading == 0:
                self.ammunition = 7
            self.reloading -= 1

    def on_collision(self, robot):
        robot.all_ammunition += 30
        robot.gun = self


class Firearm:
    id = 61
    img = QImage('texture/autogun.png')

    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r
        self.speed_x = 0
        self.speed_y = 0
        self.type_bullets = SimpleBullets
        self.reloading = 60
        self.ammunition = 3

    def possible_to_shot(self):
        if self.ammunition < 1:
            if self.reloading < 1:
                self.reloading = 30
                self.ammunition = -1
                return 3
            return 0
        self.ammunition = 0
        self.reloading = 30
        return 3

    def update(self):
        if self.ammunition < 1:
            if self.reloading == 0:
                self.ammunition = 3
            self.reloading -= 1

    def draw(self, painter):
        dx = self.x - 15
        dy = self.y - 15
        painter.drawImage(dx, dy, self.img)

    def on_collision(self, robot):
        robot.all_ammunition += 30
        robot.gun = self


class MachineGun:
    id = 62
    img = QImage('texture/machinegun.png')
    type_bullets = SimpleBullets

    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r
        self.speed_x = 0
        self.speed_y = 0
        self.reloading = 60
        self.ammunition = 999

    def possible_to_shot(self):
        return 2

    def update(self):
        if self.ammunition < 1:
            if self.reloading == 0:
                self.ammunition = 3
            self.reloading -= 1

    def draw(self, painter):
        dx = self.x - 15
        dy = self.y - 15
        painter.drawImage(dx, dy, self.img)

    def on_collision(self, robot):
        robot.all_ammunition += 30
        robot.gun = self
