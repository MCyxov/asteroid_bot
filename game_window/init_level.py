from game_window.object import *
from game_window.sourse_of_init_level import *


def init_level(game, n, robot):
    if n == 1:
        init_level_n(game, level_1)
    if n == 2:
        init_level_n(game, level_2)
    if n == 3:
        init_level_n(game, level_3)
    if n == 4:
        init_level_n(game, level_4)
    if n == 5:
        init_level_n(game, level_5)
    if n == 6:
        init_level_n(game, level_6)
    if n == 7:
        init_level_n(game, level_7)
    if n == 8:
        init_level_n(game, level_8)
    game.dynamic_field.append(robot)


def init_level_n(game, level_array):

    for i in range(len(level_array)):
        for j in range(len(level_array[i])):
            if level_array[i][j] == 0:
                game.field.set_cell(i, j, Empty(y=j, x=i))
            if level_array[i][j] == 1:
                game.field.set_cell(i, j, Block(y=j, x=i))
            if level_array[i][j] == 2:
                game.field.set_cell(i, j, UnbreakBlock(y=j, x=i))
            if level_array[i][j] == 3:
                game.field.set_cell(i, j, SuperBlock(y=j, x=i))

