import random

from PyQt5.QtGui import QColor, QImage


def draw_random_list_generation(draw_list, seed, width, height):
    for i in range(height):
        for j in range(width):
            r = random.randint(0, seed)
            if r < 2:
                draw_list[i].append(1)
            else:
                draw_list[i].append(0)
        draw_list.append([])
    return draw_list


class Empty:
    id = 1

    def __init__(self, x, y):
        self.m = 0
        self.x = x
        self.y = y
        self.draw_list = [[]]

    def draw(self, x, y, width, height, painter):
        width += x
        height += y
        painter.fillRect(x, y, width, height, QColor('#000022'))
        painter.setPen(QColor('#F5DEB3'))
        if len(self.draw_list) != height - y and len(self.draw_list[0]) != width - x:
            self.draw_list = [[]]
            draw_random_list_generation(self.draw_list, 600, width - x, height - y)
        for i in range(len(self.draw_list)):
            for j in range(len(self.draw_list[i])):
                if self.draw_list[i][j] == 1:
                    painter.drawLine(i + x, j + y, i + x, j + y)

    def update(self):
        pass

    def destroy(self, game):
        return Empty(self.x, self.y)


class Block:
    id = 2

    def __init__(self, x, y):
        self.m = 1
        self.x = x
        self.y = y
        self.draw_list = [[]]
        self.k = 0
        self.destroy_phase = 0
        self.generation = False

    def draw(self, x, y, width, height, painter):
        if not self.generation and self.destroy_phase < 10:
            self.k = random.randint(1, 4)
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid{0}.png'.format(self.k)))
            self.generation = True
        elif self.generation and self.destroy_phase < 10:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid{0}.png'.format(self.k)))
        elif 20 > self.destroy_phase >= 10:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_break1.png'.format(self.k)))
        elif 30 > self.destroy_phase >= 20:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_break2.png'.format(self.k)))
        elif 40 > self.destroy_phase >= 30:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_break3.png'.format(self.k)))

    def update(self):
        pass

    def destroy(self, game):
        self.destroy_phase += 1
        if self.destroy_phase >= 33:
            game.inventory.add_item(2)
            k = random.randint(0, 6)
            if k < 3:
                game.spawn(self.x * 30, self.y * 30, 'Simple enemy')
            elif k == 4:
                game.spawn(self.x * 30, self.y * 30, 'health')
            elif k == 5:
                game.spawn(self.x * 30, self.y * 30, 'bullets')
            elif k == 6:
                game.spawn(self.x * 30, self.y * 30, 'many')
            return Empty(self.x * 30, self.y * 30)
        return self


class UnbreakBlock:
    id = 3

    def __init__(self, x, y):
        self.m = 2
        self.x = x
        self.y = y
        self.draw_list = [[]]
        self.k = 0
        self.generation = False

    def draw(self, x, y, width, height, painter):
        if not self.generation:
            self.k = random.randint(1, 4)
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_unbreakable.png'))
            self.generation = True
        else:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_unbreakable.png'))

    def update(self):
        pass

    def destroy(self, game):
        return self


class SuperBlock:
    id = 3

    def __init__(self, x, y):
        self.m = 2
        self.x = x
        self.y = y
        self.draw_list = [[]]
        self.k = 0
        self.generation = False

    def draw(self, x, y, width, height, painter):
        if not self.generation:
            self.k = random.randint(1, 4)
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_superblock.png'))
            self.generation = True
        else:
            painter.drawImage(self.y * 30, self.x * 30, QImage('texture/asteroid_superblock.png'))

    def update(self):
        pass

    def destroy(self, game):
        game.game_status += 1
        game.next_level()


class ParticleOfDirt:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.time = 20
        self.speed_x = random.randint(-5, 5)
        self.speed_y = random.randint(-5, 5)

    def draw(self, painter):
        painter.fillRect(self.x, self.y, 3, 3, QColor('#8B4513'))

    def update(self):
        self.time -= 1
        if self.time < 0:
            return None
        return self


class ParticleOfBlood:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.time = 60
        self.speed_x = random.randint(-10, 10)
        self.speed_y = random.randint(-10, 10)

    def draw(self, painter):
        painter.fillRect(self.x, self.y, 5, 5, QColor('#F00000'))

    def update(self):
        self.time -= 1
        if self.time < 0:
            return None
        return self


class SimpleBullets:
    id = 50
    speed = 10
    damage = 2

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.time = 50
        self.speed_x = vx * self.speed
        self.speed_y = vy * self.speed

    def draw(self, painter):
        painter.fillRect(self.x, self.y, 3, 3, QColor('#FFFFFF'))

    def update(self):
        self.time -= 1
        if self.time < 0:
            return None
        return self


class EnemyBullets:
    id = 51
    speed = 20
    damage = 1

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.time = 50
        self.speed_x = vx * self.speed
        self.speed_y = vy * self.speed

    def draw(self, painter):
        painter.fillRect(self.x, self.y, 3, 3, QColor('#22FF22'))

    def update(self):
        self.time -= 1
        if self.time < 0:
            return None
        return self


class MiniBullets:
    id = 50
    speed = 20
    damage = 1

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.time = 50
        self.speed_x = vx * self.speed
        self.speed_y = vy * self.speed

    def draw(self, painter):
        painter.fillRect(self.x, self.y, 3, 3, QColor('#FF2222'))

    def update(self):
        self.time -= 1
        if self.time < 0:
            return None
        return self
