import math

from PyQt5.QtGui import QImage, QColor, QFont
from PyQt5.QtCore import Qt
from game_window.gun import *


class Renderer:
    COLOR_TABLE = {
        'white'     : 0xFFFFFF,
        'grey'      : 0x666666,
        'red'       : 0xCC6666,
        'green'     : 0x66CC66,
        'blue'      : 0x6666CC,
        'brown'     : 0xCCCC66,
        'purple'    : 0xCC66CC,
        'turquoise' : 0x66CCCC,
        'yellow'    : 0xDAAA00,
    }

    def __init__(self, painter):
        self.painter = painter
        self.arrow = QImage('texture/arrow.png')

    def render(self, game):
        square_size = self.get_square_size(game.field)

        for i in range(game.field.height):
            for j in range(game.field.width):
                cell = game.field.get_cell(i, j)
                if cell is not None:
                    self.draw_square(j * square_size[0], i * square_size[1], square_size, cell)
        self.draw_robot(square_size, game)
        self.draw_particle(game)
        self.draw_bullets(game)
        self.draw_stats(game)
        self.draw_bonus(game)

    def get_square_size(self, field):
        rect = self.painter.window()
        return 600 // field.width, 600 // field.height

    def draw_square(self, x, y, size, color_index):
        width, height = size
        painter = self.painter
        color_index.draw(x, y, width, height, painter)

    def draw_robot(self, size, game):
        width, height = size
        painter = self.painter
        game.dynamic_field[0].draw(painter)
        if (game.dynamic_field[0].x > 600 or game.dynamic_field[0].x < 0 or
            game.dynamic_field[0].y > 600 or game.dynamic_field[0].y < 0):

            x = max(min(game.dynamic_field[0].x, 585), 15)
            y = max(min(game.dynamic_field[0].y, 585), 15)
            angle = math.atan2((game.dynamic_field[0].y - 300), (game.dynamic_field[0].x - 300)) * 180 / math.pi

            painter.save()
            painter.translate(x, y)
            painter.rotate(angle)
            painter.drawImage(-16, -10, self.arrow)
            painter.restore()
        for i in range(1, len(game.dynamic_field)):
            game.dynamic_field[i].draw(painter)

    def draw_particle(self, game):
        painter = self.painter
        for i in game.particle:
            i.draw(painter)

    def draw_bullets(self, game):
        painter = self.painter
        for i in game.bullets:
            i.draw(painter)

    def draw_stats(self, game):
        painter = self.painter
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawImage(610, 10, QImage('texture/health.png'))
        painter.drawImage(610, 50, QImage('texture/ammo.png'))
        painter.drawImage(610, 90, QImage('texture/coin.png'))
        painter.drawText(650, 10, 100, 30, Qt.AlignLeft, str(game.dynamic_field[0].health))
        painter.drawText(650, 50, 100, 30, Qt.AlignLeft, str(game.dynamic_field[0].all_ammunition))
        painter.drawText(650, 90, 100, 30, Qt.AlignLeft, str(game.dynamic_field[0].many))
        painter.drawText(650, 130, 100, 30, Qt.AlignLeft, str(game.dynamic_field[0].gun.ammunition))
        painter.drawImage(610, 130, QImage(game.dynamic_field[0].gun.img))

    def draw_bonus(self, game):
        painter = self.painter
        for i in game.bonus:
            i.draw(painter)
