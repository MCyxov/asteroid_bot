import math

from PyQt5.QtGui import QCursor

from game_window.gun import *


class Robot:
    def __init__(self, x, y, r, window):
        self.window = window
        self.x = x
        self.y = y
        self.r = r
        self.speed_of_x = 0
        self.speed_of_y = 0
        self.angle = 0
        self.health = 10
        self.all_ammunition = 30
        self.gun = Pistol(0, 0, 0)
        self.many = 0
        self.img1_r = QImage('texture/left_right.png')
        self.img2_r = QImage('texture/circle_right.png')
        self.img3_r = QImage('texture/right_right.png')
        self.img1_l = QImage('texture/right_left.png')
        self.img2_l = QImage('texture/circle_left.png')
        self.img3_l = QImage('texture/left_left.png')

    def draw(self, painter):
        self.gun.update()
        xd = self.x - 15
        yd = self.y - 15
        if self.speed_of_x >= 0:
            self.angle += self.speed_of_x * 25 + 5
            painter.drawImage(xd, yd, self.img1_r)

            painter.save()
            painter.translate(self.x, self.y)
            painter.rotate(self.angle)
            painter.drawImage(-15, -15, self.img2_r)
            painter.restore()

            x = QCursor.pos().x() - self.window.x() - self.x
            y = QCursor.pos().y() - self.window.y() - self.y
            angle2 = math.atan2(y, x) * 180 / math.pi
            painter.save()
            painter.translate(self.x, self.y)
            painter.rotate(angle2)
            painter.drawImage(-15, -15, self.img3_r)
            painter.restore()
        else:
            self.angle += self.speed_of_y * 25 + 5
            painter.drawImage(xd, yd, self.img1_l)
            painter.save()
            painter.translate(self.x, self.y)
            painter.rotate(self.angle)
            painter.drawImage(-15, -15, self.img2_l)
            painter.restore()

            x = QCursor.pos().x() - self.window.x() - self.x
            y = QCursor.pos().y() - self.window.y() - self.y
            angle2 = math.atan2(y, x) * 180 / math.pi
            painter.save()
            painter.translate(self.x, self.y)
            painter.rotate(angle2)
            painter.drawImage(-15, -15, self.img3_l)
            painter.restore()

    def update(self):
        pass

    def hit(self, game):
        if self.health == 1:
            game.game_over()
        else:
            self.health -= 1

    def shot(self):
        if self.all_ammunition < 1:
            return 0
        return self.gun.possible_to_shot()
