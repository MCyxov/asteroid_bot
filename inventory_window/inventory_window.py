from PyQt5.QtWidgets import QMainWindow, QDesktopWidget
from PyQt5.QtGui import QPainter, QImage, QColor, QPen, QBrush, QFont
from PyQt5.QtCore import Qt


class Item:
    def __init__(self, type, img, count=1):
        self.img = img
        self.count = count
        self.x = 0
        self.y = 0
        self.type = type

    def draw(self, painter, x, y):
        self.x = x
        self.y = y

        painter.setOpacity(0.7)
        painter.setBrush(QBrush(QColor(0, 100, 220)))
        painter.setPen(QPen(Qt.transparent))
        painter.drawRect(self.x, self.y - 2, 100, 34)
        painter.setOpacity(1.0)

        painter.drawImage(x, y - 2, self.img)
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 60, self.y, 100, 30, Qt.AlignLeft, 'x' + str(self.count))


class InventoryWindow(QMainWindow):

    Images = [
            QImage('texture/circle_right.png'),
            QImage('texture/right_right.png'),
            QImage('texture/asteroid1.png')
        ]

    def __init__(self):
        super().__init__()
        self.items = [Item(0, self.Images[0]), Item(1, self.Images[1])]
        self.background = QImage('texture/inventory.png')
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Inventory')
        self.resize(500, 500)
        self.center()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setBrush(QBrush(QColor(5, 2, 1)))
        painter.drawImage(0, 0, self.background)
        for i in range(len(self.items)):
            self.items[i].draw(painter, 30, 60 + i * 36)
        self.update()

    def add_item(self, type):
        f = False
        for i in range(len(self.items)):
            if self.items[i].type == type:
                self.items[i].count += 1
                f = True
        if not f:
            img = self.Images[type]
            self.items.append(Item(type, img))

    def center(self):
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) // 2,
                  (screen.height() - size.height()) // 2)