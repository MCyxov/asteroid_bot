#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon

from main_window.main_window import Start

if __name__ == '__main__':
    app = QApplication([])
    app.setWindowIcon(QIcon('icon.png'))
    game = Start()
    sys.exit(app.exec_())
