from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QFrame, QPushButton, QVBoxLayout

from game_window.gui import SnakeWindow


class Start(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.secondWindow = None

        self.init_ui()

    def init_ui(self):
        start_game = QPushButton("Начать игру", self)
        start_game.clicked.connect(self.start_game)

        back = QPushButton("Выйти", self)
        back.clicked.connect(self.back)

        horizontal = QVBoxLayout()
        horizontal.addWidget(start_game)
        horizontal.addWidget(back)
        self.setLayout(horizontal)

        self.setGeometry(400, 400, 400, 250)
        self.setWindowTitle('menu')
        self.show()

    def start_game(self):
        self.hide()
        self.secondWindow = SnakeWindow()

    def back(self):
        self.close()
