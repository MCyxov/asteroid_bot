import math

from game_window.all_bonus import *
from game_window.enemy import Stranger
from game_window.field import Field
from game_window.gun import Pistol, Firearm
from shop.init_level import init_level
from game_window.object import ParticleOfDirt, SimpleBullets
from inventory_window.inventory_window import InventoryWindow
from shop.products import productPistol, productAmmo, productHealth, productFirearm, productMachineGun


class Game:

    friction_koef = 0.8
    pump_koef = 0.7
    g = 0.008
    center = (300, 300)

    def __init__(self, height, wight, robot):
        self.game_status = 1
        self.inventory = InventoryWindow()
        self.window = None
        self.height = height
        self.wight = wight
        self.count_y = 30
        self.is_paused = False
        self.field = Field(width=self.wight, height=self.height, game=self)
        self.id_bullets = [51, 0]
        self.is_enemy = 0
        self.dynamic_field = []
        self.particle = []
        self.bullets = []
        self.bonus = []
        self.dynamic_field.append(robot)

    def start(self, window, n, gui):
        self.gui = gui
        self.window = window
        self.game_status = n
        self.spawn(40, 360, 'ammo')
        self.spawn(140, 360, 'health')
        self.spawn(240, 360, 'Pistol')
        self.spawn(340, 360, 'Firearm')
        self.spawn(440, 360, 'machinegun')
        init_level(self)

    def break_of_robot(self):
        self.dynamic_field[0].speed_of_x = 0
        self.dynamic_field[0].speed_of_y = 0

    def update(self):
        if self.is_paused:
            self.break_of_robot()
            return
        self.real_go()
        self.physics_of_particle()
        self.physics_of_bullets()
        self.physics_of_enemy()
        self.physics_of_bonus()

    def pause(self):
        self.is_paused = not self.is_paused

    def go(self, how):
        PATTERNS = {
            'u': (0, -1),
            'd': (0, 1),
            'l': (-1, 0),
            'r': (1, 0),
        }

        SPEED_X = PATTERNS[how][0]
        SPEED_Y = PATTERNS[how][1]

        if self.dynamic_field[0].speed_of_x < 30:
            self.dynamic_field[0].speed_of_x += SPEED_X
        if self.dynamic_field[0].speed_of_y < 30:
            self.dynamic_field[0].speed_of_y += SPEED_Y

    def real_go(self):
        if 0.05 < self.dynamic_field[0].speed_of_x < 0.05:
            self.dynamic_field[0].speed_of_x = 0
        if 0.05 < self.dynamic_field[0].speed_of_y < 0.05:
            self.dynamic_field[0].speed_of_y = 0
        if self.field.possible_to_go(self.dynamic_field[0].x + self.dynamic_field[0].speed_of_x,
                                     self.dynamic_field[0].y + self.dynamic_field[0].speed_of_y, self.dynamic_field[0].r):
            self.dynamic_field[0].x += self.dynamic_field[0].speed_of_x
            self.dynamic_field[0].y += self.dynamic_field[0].speed_of_y
        else:
            if self.field.possible_to_go(self.dynamic_field[0].x + self.dynamic_field[0].speed_of_x,
                                         self.dynamic_field[0].y, self.dynamic_field[0].r):
                self.dynamic_field[0].x += self.dynamic_field[0].speed_of_x
                self.dynamic_field[0].speed_of_x *= self.friction_koef
                self.dynamic_field[0].speed_of_y = 0
            elif self.field.possible_to_go(self.dynamic_field[0].x,
                                           self.dynamic_field[0].y + self.dynamic_field[0].speed_of_y, self.dynamic_field[0].r):
                self.dynamic_field[0].y += self.dynamic_field[0].speed_of_y
                self.dynamic_field[0].speed_of_y *= self.friction_koef
                self.dynamic_field[0].speed_of_x = 0
            else:
                self.dynamic_field[0].speed_of_x = 0
                self.dynamic_field[0].speed_of_y = 0

    def destroy(self):
        x = self.dynamic_field[0].x
        y = self.dynamic_field[0].y
        if not self.field.possible_to_go_for_point(x + 16, y):
            self.field.destroy(x + 16, y, self)
            self.add_particle(x + 16, y)
        if not self.field.possible_to_go_for_point(x - 16, y):
            self.field.destroy(x - 16, y, self)
            self.add_particle(x - 16, y)
        if not self.field.possible_to_go_for_point(x, y + 16):
            self.field.destroy(x, y + 16, self)
            self.add_particle(x, y + 16)
        if not self.field.possible_to_go_for_point(x, y - 16):
            self.field.destroy(x, y - 16, self)
            self.add_particle(x, y - 16)

    def physics_of_particle(self):
        for i in self.particle:
            if self.field.possible_to_go_for_point(i.x + i.speed_x, i.y + i.speed_y):
                i.x += i.speed_x
                i.y += i.speed_y
            elif self.field.possible_to_go_for_point(i.x + i.speed_x, i.y):
                i.x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go_for_point(i.x, i.y + i.speed_y):
                i.speed_x = -i.speed_x * self.pump_koef
                i.y += i.speed_y
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if i.update() is None:
                self.particle.pop(self.particle.index(i))

    def add_particle(self, x, y):
        self.particle.append(ParticleOfDirt(x, y))

    def shot(self, x2, y2):
        k = self.dynamic_field[0].shot()
        for i in range(k):
            x1 = self.dynamic_field[0].x
            y1 = self.dynamic_field[0].y
            vx = (x2 - x1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            vy = (y2 - y1) / math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            self.dynamic_field[0].speed_of_x -= vx
            self.dynamic_field[0].speed_of_y -= vy
            self.bullets.append(SimpleBullets(x1 - vx * i * 10, y1 - vy * i * 10, vx * 20, vy * 20))

    def physics_of_bullets(self):
        for i in self.bullets:
            self.id_bullets = [i.id, self.bullets.index(i)]
            if i.update() is None:
                self.bullets.pop(self.bullets.index(i))
            if self.field.possible_to_go_for_point(i.x + i.speed_x, i.y + i.speed_y):
                i.x += i.speed_x
                i.y += i.speed_y
            elif self.field.possible_to_go_for_point(i.x + i.speed_x, i.y):
                i.x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go_for_point(i.x, i.y + i.speed_y):
                i.speed_x = -i.speed_x * self.pump_koef
                i.y += i.speed_y
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if self.collision_with_robot(i.x, i.y) and self.id_bullets[0] == 51:
                self.bullets.pop(self.id_bullets[1])
                self.dynamic_field[0].hit(self)
            self.id_bullets = [51, 0]

    def physics_of_enemy(self):
        for j in range(1, len(self.dynamic_field)):
            self.is_enemy = j
            i = self.dynamic_field[j]
            if self.field.possible_to_go(i.x + i.speed_x, i.y + i.speed_y, i.r):
                self.dynamic_field[j].x += i.speed_x
                self.dynamic_field[j].y += i.speed_y
            elif self.field.possible_to_go(i.x + i.speed_x, i.y, i.r):
                self.dynamic_field[j].x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go(i.x, i.y + i.speed_y, i.r):
                i.speed_x = -i.speed_x * self.pump_koef
                self.dynamic_field[j].y += i.speed_y
            else:
                i.speed_y = 0
                i.speed_x = 0
            i.shot(self)
        self.is_enemy = -1

    def physics_of_bonus(self):
        for j in range(len(self.bonus)):
            try:
                i = self.bonus[j]
            except IndexError:
                continue
            if self.field.possible_to_go(i.x + i.speed_x, i.y + i.speed_y, i.r):
                self.bonus[j].x += i.speed_x
                self.bonus[j].y += i.speed_y
            elif self.field.possible_to_go(i.x + i.speed_x, i.y, i.r):
                self.bonus[j].x += i.speed_x
                i.speed_y = -i.speed_y * self.pump_koef
            elif self.field.possible_to_go(i.x, i.y + i.speed_y, i.r):
                i.speed_x = -i.speed_x
                self.bonus[j].y += i.speed_y * self.pump_koef
            else:
                i.speed_y = -i.speed_y * self.pump_koef
                i.speed_x = -i.speed_x * self.pump_koef
            if self.collision_with_robot(self.bonus[j].x, self.bonus[j].y, i.r):
                if self.dynamic_field[0].many - self.bonus[j].cost >= 0:
                    self.bonus[j].on_collision(self.dynamic_field[0])
                    print(self.dynamic_field[0].many)
                    self.bonus.pop(j)

    def spawn(self, x, y, spawn_type):
        if spawn_type == 'health':
            self.bonus.append(productHealth(y + 15, x + 15, 5))
        elif spawn_type == 'bullets':
            self.bonus.append(productAmmo(y + 15, x + 15, 5))
        elif spawn_type == 'Simple enemy':
            self.dynamic_field.append(Stranger(x + 15, y + 15, 10))
        elif spawn_type == 'Firearm':
            self.bonus.append(productFirearm(y + 15, x + 15, 5))
        elif spawn_type == 'Pistol':
            self.bonus.append(productPistol(y + 15, x + 15, 5))
        elif spawn_type == 'machinegun':
            self.bonus.append(productMachineGun(y + 15, x + 15, 5))
        elif spawn_type == 'ammo':
            self.bonus.append(productAmmo(y + 15, x + 15, 5))
        elif spawn_type == 'many':
            self.bonus.append(AddMany(y + 15, x + 15, 5))

    def game_over(self):
        self.is_paused = True
        self.game_status = -1

    def collision_with_robot(self, xp, yp, r=0):
        xr = self.dynamic_field[0].x
        yr = self.dynamic_field[0].y
        if (xr - xp) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp + r) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp) ** 2 + (yr - yp + r) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp - r) ** 2 + (yr - yp) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        if (xr - xp) ** 2 + (yr - yp - r) ** 2 <= self.dynamic_field[0].r ** 2:
            return True
        return False

    def move_to(self, x2, y2):
        x1 = self.dynamic_field[0].x
        y1 = self.dynamic_field[0].y
        vx = (x2 - x1)
        vy = (y2 - y1)
        self.dynamic_field[0].speed_of_x += vx // 35
        self.dynamic_field[0].speed_of_y += vy // 35

    def next_level(self):
        pass
