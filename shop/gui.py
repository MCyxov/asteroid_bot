from PyQt5.QtCore import pyqtSignal, QBasicTimer
from PyQt5.QtWidgets import QFrame, QDesktopWidget, QMessageBox

from shop.game import Game
from .rendor import *


class Board(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self, parent1, robot, n):
        super().__init__()
        self.n = n
        self.parent1 = parent1
        self.robot = robot
        self.start_level(1)
        self.setWindowTitle('Shop')
        self.resize(700, 600)
        self.center()
        self.show()
        #  self.parent = parent
        self.UPDATE_INTERVAL = 60
        self.mode = 0

        self.timer = QBasicTimer()
        self.timer.start(self.UPDATE_INTERVAL, self)

        self.setFocusPolicy(Qt.StrongFocus)

        # self.setCentralWidget(self)

    def center(self):
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) // 2,
                  (screen.height() - size.height()) // 2)

    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            self.game.update()
            self.update()
            self.update_status()
        else:
            super().timerEvent(event)

    def start_level(self, n):
        game = Game(20, 20, self.robot)
        game.start(self, n, self)
        self.game = game

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setViewport(self.contentsRect())

        renderer = Renderer(painter)
        renderer.render(self.game)

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Escape:
            self.exit()
        if key == Qt.Key_Right or key == Qt.Key_D:
            self.game.go('r')
        if key == Qt.Key_Left or key == Qt.Key_A:
            self.game.go('l')
        if key == Qt.Key_Up or key == Qt.Key_W:
            self.game.go('u')
        if key == Qt.Key_Down or key == Qt.Key_S:
            self.game.go('d')
        if key == Qt.Key_Space:
            self.game.break_of_robot()
            self.game.destroy()
        if key == Qt.Key_Q:
            self.game.inventory.show()
        if key == Qt.Key_0:
            self.mode = 0
        if key == Qt.Key_1:
            self.mode = 1

    def exit(self):
        from game_window.gui import SnakeWindow
        self.hide()
        self.win = SnakeWindow(self.n, self.robot)

    def mousePressEvent(self, e):
        if e.buttons() == Qt.LeftButton:
            self.update()
            self.game.shot(e.pos().x(), e.pos().y())
        if e.buttons() == Qt.RightButton:
            self.update()
            self.game.move_to(e.pos().x(), e.pos().y())

    def update_status(self):
        status = 'RUN'
        if self.game.is_paused:
            status = 'PAUSED'
        if self.game.game_status == -1:
            QMessageBox.information(self, "asteroid bot", "YOU DIED")
            self.close()
        self.statusUpdated.emit(status)
