from PyQt5.QtGui import QColor, QImage, QFont
from PyQt5.QtCore import Qt
from game_window.all_bonus import AddHealth1, AddAmmo1
from game_window.gun import Pistol, Firearm, MachineGun


class productHealth(AddHealth1):
    def __init__(self, x, y, r):
        super().__init__(x, y, r)
        self.cost = 2

    def draw_cost(self, painter):
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 30, self.y - 10, 100, 30, Qt.AlignLeft, str(self.cost))

    def on_collision(self, robot):
        if robot.many > 0:
            robot.many -= self.cost
            super().on_collision(robot)


class productAmmo(AddAmmo1):

    def __init__(self, x, y, r):
        super().__init__(x, y, r)
        self.cost = 2

    def draw_cost(self, painter):
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 30, self.y - 10, 100, 30, Qt.AlignLeft, str(self.cost))

    def on_collision(self, robot):
        if robot.many > 0:
            robot.many -= self.cost
            super().on_collision(robot)


class productPistol(Pistol):

    def __init__(self, x, y, r):
        super().__init__(x, y, r)
        self.cost = 2

    def draw_cost(self, painter):
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 30, self.y - 10, 100, 30, Qt.AlignLeft, str(self.cost))

    def on_collision(self, robot):
        if robot.many > 0:
            robot.many -= self.cost
            super().on_collision(robot)


class productFirearm(Firearm):

    def __init__(self, x, y, r):
        super().__init__(x, y, r)
        self.cost = 2

    def draw_cost(self, painter):
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 30, self.y - 10, 100, 30, Qt.AlignLeft, str(self.cost))

    def on_collision(self, robot):
        if robot.many > 0:
            robot.many -= self.cost
            super().on_collision(robot)


class productMachineGun(MachineGun):

    def __init__(self, x, y, r):
        super().__init__(x, y, r)
        self.cost = 4

    def draw_cost(self, painter):
        painter.setPen(QColor(200, 250, 250))
        painter.setFont(QFont('Decorative', 18))
        painter.drawText(self.x + 30, self.y - 10, 100, 30, Qt.AlignLeft, str(self.cost))

    def on_collision(self, robot):
        if robot.many > 0:
            robot.many -= self.cost
            super().on_collision(robot)
